import Vue from 'vue'
import Router from 'vue-router'
import MainLayout from '@/layouts/MainLayout'
import Index from '@/views/Customer/Index'
import Slide from '@/views/Admin/Slide'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MainLayout',
      component: MainLayout
    },
    {
      path: '/index',
      name: 'Index',
      component: Index
    },
    {
      path: '/slide',
      name: 'Slide',
      component: Slide
    },
  ]
})
